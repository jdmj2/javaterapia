$(document).on("click", "#botonDonar" , function(e){ 
    window.location = "vistadonante.jsp";
});

$(document).on("click", "#botonDonarXS" , function(e){
    window.location = "vistadonante.jsp";
});

  $(document).on("click", "#displayNuevaPeticion" , function(e){
      
    $("#formNuevaPeticion").toggleClass("hideForm displayForm");

    // if($("#formNuevaPeticion").hasClass("hideForm")){
    //     $("#formNuevaPeticion").removeClass("hideForm");
    //     $("#formNuevaPeticion").addClass("displayForm");
    // }
    // else if($("#formNuevaPeticion").hasClass("displayForm")){
    //     $("#formNuevaPeticion").removeClass("displayForm");
    //     $("#formNuevaPeticion").addClass("hideForm");
    // }
})

// Cambia el Icono de la categoria del juego 
// en función de la opción seleccionada en el form
$(document).on("change", "#opcionesCategoria" , function(e){

if (this.value == "videojuegos") {
    $(".espacioIconoCategoria").css("background-color", "white");
    $(".espacioIconoCategoria").css("border", "none");
    $("#iconoCategoriaJuego").addClass("fas");
    $("#iconoCategoriaJuego").removeClass("fa-dice");
    $("#iconoCategoriaJuego").removeClass("fa-futbol");
    $("#iconoCategoriaJuego").addClass("fa-gamepad");
}
else if (this.value == "juegos de mesa") {
    $(".espacioIconoCategoria").css("background-color", "white");
    $(".espacioIconoCategoria").css("border", "none");
    $("#iconoCategoriaJuego").addClass("fas");
    $("#iconoCategoriaJuego").removeClass("fa-gamepad");
    $("#iconoCategoriaJuego").removeClass("fa-futbol");
    $("#iconoCategoriaJuego").addClass("fa-dice");
}
else if (this.value == "juegos deportivos") {
    $(".espacioIconoCategoria").css("background-color", "white");
    $(".espacioIconoCategoria").css("border", "none");
    $("#iconoCategoriaJuego").addClass("fas");
    $("#iconoCategoriaJuego").removeClass("fa-dice");
    $("#iconoCategoriaJuego").removeClass("fa-gamepad");
    $("#iconoCategoriaJuego").addClass("fa-futbol");
}


});

