<%@page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.codesplai.javaterapia.*" %>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>JavaTerapia | Donamos diversión</title>

    <!--Google Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather|Montserrat|Roboto|Sacramento" rel="stylesheet">
    <!--CSS Stylesheets-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <!--Favicon-->
    <link rel="icon" href="favicon.ico">
    <!--Font Awesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


</head>

<style>



.fichaPeticion{

    border-radius: 20px;
    min-height: 250px;
    margin-top: 5%;
    transition: all 200ms ease;
    box-shadow: 2px 2px 4px silver;
}

.fichaPeticion:hover{

    box-shadow: 4px 4px 8px grey;
}

.fichaEnProceso{

    filter: opacity(0.5);
    box-shadow: none;

}

.fichaEnProceso:hover{
    box-shadow: none;
}



</style>

<body>
<%@include file="/partial/login.jsp"%>
<%@include file="/partial/menu.jsp"%>
<div class="container-fluid">

        <h2 class="realizarDonacion">Realizar donación <i class="fas fa-hand-holding"></i></h2>
        <div class="row">
        <div class=" filtrosHC form-group col-10">
<div class="form-group">
<form action="vistadonante.jsp" method="GET">

                <p>Filtrar por:</p>
                <div class="row">
                    <div class="col-12 col-md-6 ">

                        <select name ="hospital"class="custom-select custom-select-lg mb-3">

                            <option selected value="0">Selecciona un Hospital</option>
                            <% for (Hospitales hosp : ControllerHospitales.getAll()) { %>

                            <option value="<%= hosp.getId() %>"><%= hosp.getNombreHospital() %></option>
                               <% } %>
                        </select>

                    </div>
                    <div class="col-12 col-md-6">

                        <select  class="custom-select custom-select-lg mb-3" name="categoria_id" id="categoria_id">
                            <option selected value="nada">Selecciona una categoría</option>
                            <option value="videojuegos">Videojuegos</option>
                            <option value="juegos de mesa">Juegos de Mesa</option>
                            <option value="juegos deportivos">Juegos deportivos</option>
                        </select>
                    </div>
                </div>
    <button type="submit" value="Submit" class="btn btn-info">Aplicar</button>

                    <%-- <input type="hidden" name="id" value="<%=id %>" > --%>
                        </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">

                    <%
if("GET".equalsIgnoreCase(request.getMethod())){
                    String id = request.getParameter("hospital");
                    String categoria = request.getParameter("categoria_id");
                       System.out.println(categoria);
                       int id_numerico = Integer.parseInt(id);
                int test = 1;

                    for (Peticiones pet : ControllerPeticiones.getAllPeticiones(id_numerico, categoria)) { %>
                    <% if  (pet.getEstado()!=2) {   %>
                    <% Hospitales u = ControllerHospitales.getId(pet.getIdHospitales());%>
                    <%  String fichaPendiente = "";
                        String disabled = "";

                        if(pet.getEstado()==1){

                            fichaPendiente = "fichaEnProceso";
                            disabled = "disabled";
                        }


                    %>

                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 mt-3 mb-3">
                        <div class="fichaPeticion <%=fichaPendiente%> card">
                            <h5 class="card-header"><%= u.getNombreHospital() %></h5>
                            <div class="card-body peticionestarjeta">
                                <h5 class="card-title"><%= pet.getNombreCategoria()%><i class="fas <%= ControllerPeticiones.getCategoria(pet.getNombreCategoria()) %>"> </i></h5>
                                <p class="card-text "><%= pet.getDescripcion() %></p>
                            </div>
                            <div class="card-body">
                            <form action="confirmacion.jsp" method="GET">
                            <input type="hidden" name="idpeticion" value="<%= pet.getId()%>">
                            <input type="submit" class="btn btn-lg botonFicha btn-primary" value="Donar" <%=disabled%> >
                            </form>
                            </div>
                        </div>
                    </div>



                    <% }}} %>


    </div>
    <div>
    <script>
        var http = new XMLHttpRequest();
        http.onreadystatechange = function(){
            if (http.readyState == 4 && http.status == 200){
                document.body.innerHTML += http.responseText; // you can change the target. As long as you append http.responseText. It will work fine.
            }
        };
        http.open('GET', 'partial/footer.html', true); // note: link the footer.html
        http.send();
    </script>
    </div>

        <!--Bootstrap Scripts-->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
</body>

</html>
