
<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.codesplai.javaterapia.*" %>

<%
    String idPeticiones = request.getParameter("idpeticion");
%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>JavaTerapia | Donamos diversión</title>

    <!--Google Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather|Montserrat|Roboto|Sacramento" rel="stylesheet">
    <!--CSS Stylesheets-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <!--Favicon-->
    <link rel="icon" href="favicon.ico">
    <!--Font Awesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>

<body>
<%@include file="/partial/login.jsp"%>
<%@include file="/partial/menu.jsp"%>
    <!---------------  CUERPO -------------->
    <div class="container-fluid">
    <h2 class="realizarDonacion">Realizar donación <i class="fas fa-hand-holding"></i></h2>
    <div class="row">
            <div class="confirmarFichas form-group col-10">
                        <form action="agradecimiento.jsp" method="GET">
                            <div class="form-group"> 
                                <label for="nombreDonante">Nombre</label>
                                <input type="name" required size="40" class="form-control" name="nombreDonante" id="nombreDonante"
                                     aria-describedby="emailHelp" placeholder="Nombre">
                            </div>
                        <div class="form-group"> 
                            <label for="emailDonante">Email</label>
                            <input type="email" size="40" required class="form-control" name="emailDonante" id="emailDonante"
                                aria-describedby="emailHelp" placeholder="Email">
                        </div>
                <input type="hidden" name="idpeticion" value="<%= idPeticiones %>">
            
                <!----------------------------- POP UP REGLAS DONACiÓN--------------------------------->
                <a id="recordatorioNormas" href="" data-toggle="modal" data-target="#reglasDonacion">Antes de finalizar
                        revisa nuestras normas. </a> <br><br>
                <%-- <div class=" polPrivacidad form-group form-check"> --%>
                    <input required type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">
                        He leído y acepto la <strong><a id="normasDonacion" href="" data-toggle="modal"
                                data-target="#politicaPrivacidad">política de privacidad</a></strong>
                    </label>
                <br/><br>
                <button type="submit" class="btn btn-info">Aceptar</button>
                </div>
    </form>
    </div>
    </div>
    

            <% String id = request.getParameter("idpeticion");
                int id_numerico = Integer.parseInt(id);
                int test = 1;
  for( Peticiones pet : ControllerPeticiones.getPeticionById(id_numerico)){
      Hospitales u = ControllerHospitales.getId(pet.getIdHospitales());%>
  <div class="col-12 col-sm-6 col-md-4 col-lg-3 mt-3 mb-3" >
   <div class="fichaPeticion card" style="margin: 0 auto">
                            <h5 class="card-header"><%= u.getNombreHospital() %></h5>
                            <div class="card-body peticionestarjeta">
                                <h5 class="card-title"><%= pet.getNombreCategoria()%><i class="fas <%= ControllerPeticiones.getCategoria(pet.getNombreCategoria()) %>"> </i></h5>
                                <p class="card-text "><%= pet.getDescripcion() %></p>
                            </div>
                            <div class="card-body">
                            <form action="confirmacion.jsp" method="GET">
                            <a href="#" class="btn btn-lg disabled botonFicha btn-primary">DONAR</a>
                            <input type="hidden" name="idpeticion" value="<%= pet.getId()%>">
                            </form>
                            </div>
                        </div>
                    </div>
    <% }%>

    <!-- Modal -->
                <div class="modal fade" id="reglasDonacion" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Recuerda:</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                                <ul>
                    <li> Antes de donar un juego o juguete, <strong>comprueba siempre que
                         funcione</strong>, ya que
                         los niños se ponen tristes si luego no pueden jugar. </li>
                    <li> Y por favor, si puedes limpiarlo/s, ¡te lo agradeceremos mucho!,
                          todo es donado a peques que están en hospitales y <strong>es fundamental que
                          todo
                          vaya limpio para evitar contagios</strong>. </li>
                    <li>Y por úlltimo, para que tu donación llegue "sana y salva"
                        necesitamos que la metas en una caja. Todos tenemos cajas de zapatos en casa.
                        ¿No te parece una solución estupenda? </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Entendido</button>
            </div>
        </div>
    </div>
</div>




    <!--Bootstrap Scripts-->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script>
        var http = new XMLHttpRequest();
        http.onreadystatechange = function(){
            if (http.readyState == 4 && http.status == 200){
                document.body.innerHTML += http.responseText; // you can change the target. As long as you append http.responseText. It will work fine.
            }
        };
        http.open('GET', 'partial/footer.html', true); // note: link the footer.html
        http.send();
    </script>
</body>

</html>
