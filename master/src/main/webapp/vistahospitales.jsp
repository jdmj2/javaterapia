<%@page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.codesplai.javaterapia.*" %>


<%
    if("POST".equalsIgnoreCase(request.getMethod())){

        request.setCharacterEncoding("UTF-8");

        String categoriaJuego = request.getParameter("opcionesCategoria");
        String descripcionPeticion = request.getParameter("descripcionPeticion");
        int idHospitalPeticion = Integer.parseInt(request.getParameter("idHospital"));
        System.out.println(categoriaJuego+descripcionPeticion+idHospitalPeticion);
        Peticiones fichaPeticion = new Peticiones(categoriaJuego, descripcionPeticion, idHospitalPeticion);
        
        ControllerPeticiones.savePeticion(fichaPeticion);
        
    }
            
    %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>javaterapia</title>

    <!--Google Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,900|Ubuntu" rel="stylesheet">
    <!--CSS Stylesheets-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <!--Favicon-->
    <link rel="icon" href="favicon.ico">
    <!--Font Awesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


</head>

<body>
<%@include file="/partial/login.jsp"%>
<%@include file="/partial/menu.jsp"%>


    <!--Bootstrap Scripts-->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    
</body>

</html>



