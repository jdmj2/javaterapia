<%@page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.codesplai.javaterapia.*" %>


<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>JavaTerapia | Donamos diversión</title>

    <!--Google Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather|Montserrat|Roboto|Sacramento" rel="stylesheet">
    <!--CSS Stylesheets-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <!--Favicon-->
    <link rel="icon" href="favicon.ico">
    <!--Font Awesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>


<body>
<%@include file="/partial/login.jsp"%>
<%@include file="/partial/menu.jsp"%>
    
<section id="carouselPrimero" class="d-none d-sm-block">
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <link rel="icon" href="img/JavaTerapiaIconoMesa de trabajo 1IconoJavaTerapia.ico">
            <a href="vistadonante.jsp?hospital=0&categoria_id=nada" id="botonDonar" role="button"
                class="btn btn-info BotonCarousel justify-content-around">DONAR</a>
            <div class="carousel-item active">
                <img src="img/caruseljpg.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="img/carusel2.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="img/carusel3.jpg" class="d-block w-100" alt="...">
            </div>
        </div>
    </div>
</section>
<div class="btnDonarXS">
<a href="vistadonante.jsp?hospital=0&categoria_id=nada" id="botonDonarXS" role="button"
                class="btn btn-info BotonCarousel justify-content-around d-block d-sm-none">DONAR</a>
            </div>

<section id="nosotrosIndex">

    <div class="container-fluid">
        <br>

        <div class="row d-flex justify-content-around mb-5 mt-5">
            <div class="col-md-5 col-10">
                <h1 id="nEquipo" class="titulos">nuestro equipo</h1>
                <hr>
                <p class="intro"> Somos <strong>JDMJ</strong>, un equipo de desarrolladores web de los que nace
                    <strong>Javaterapia</strong>, iniciativa con la idea de
                    intentar hacer más agradable y llevadera la estacia de los niños hospitalizados
                    mediante las <strong>donaciones</strong> de diferentes tipos de <strong>juguetes</strong>. Dichas donaciones
                    se ofrecen a los diferentes hospitales registrados en nuestra plataforma cubriendo su necesidad,
                    e incentivando así a los usuarios a realizar donaciones mediante un proceso fácil e intuitivo.</p>
            </div>
            <div class="col-md-5 col-10">
                <img class="fotosNosotros img-fluid" src="img/quienessomos.jpg" alt="Nuestro equipo">
            </div>
        </div>
        <div class="row d-flex justify-content-around mb-5 mt-5">
            <div class="col-md-5 col-10 order-2 order-md-1">
                <img class="fotosNosotros img-fluid"src="img/quehacemos.jpg" alt="Nuestro trabajo">
            </div>
            <div class="col-md-5 col-10 order-1 order-md-2">
                <h1 id="nTrabajo" class="titulos">nuestro trabajo</h1>
                <hr>
                <p class="intro"> Mediante una interfaz <strong>sencilla e intuitiva</strong>, facilitar tanto a los hospitales como a
                    los donantes la puesta en común de necesidades y posibles donaciones. <br>
                    Los niños hospitalizados al estar fuera de casa, alejados de los suyos y en un entorno desconocido,
                    pueden sentirse intimidados por dicha experiencia. Es por eso que los videojuegos, tablets y los
                    diferentes tipos de juegos y juguetes les brindan la posibilidad de divertirse y disfrutar dentro
                    del hospital.
                    Y se convierten en un medio fundamental de desconexión, haciendo que se olviden de dónde están, al
                    menos durante el tiempo que dure la partida.</p>
            </div>
        </div>

    </div>
</section>

    <!-- Experiencias -->
    <section id="testimonials">

        <div id="testimonial-carousel" class="carousel slide" data-ride="false">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <h2 class="maria testimonial-text">"Colaborar con Javaterapia es un regalo. Y significa poder achuchar 
                        desde la distancia a todos los niños y ayudarles a llenar sus días de juego y de ilusión, 
                        lo que merecen todos los niños del mundo."
                    </h2>
                    <em>María, Badalona</em>
                </div>
                <div class="carousel-item">
                    <h2 class="testimonial-text">"Gracias porque la labor que realizáis es encomiable!
                        Son días grises y los convertís en “días Arco Iris”."</h2>
                    <em>Marc, Barcelona</em>
                </div>
                <div class="carousel-item">
                        <h2 class="testimonial-text">"¡Un verdadero lujazo colaborar con Javaterapia!
                            Estos niños-héroes nos dan una lección de vida y se lo merecen todo." </h2>
                        <em>Marta, Barcelona</em>
                    </div>
            </div>
            <a class="carousel-control-prev" href="#testimonial-carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <!-- El elemento "a" contiene el boton de siguiente y anterior (para los testimonios del carousel), el SPAN. -->
            <a class="carousel-control-next" href="#testimonial-carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

    </section>

    <!--Bootstrap Scripts-->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script>
        var http = new XMLHttpRequest();
        http.onreadystatechange = function(){
            if (http.readyState == 4 && http.status == 200){
                document.body.innerHTML += http.responseText; // you can change the target. As long as you append http.responseText. It will work fine.
            }
        };
        http.open('GET', 'partial/footer.html', true); // note: link the footer.html
        http.send();
    </script>
</body>

</html>
