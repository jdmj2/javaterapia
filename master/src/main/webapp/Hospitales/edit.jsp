<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.codesplai.javaterapia.*" %>

<%
    Hospitales hosp = null; 
    request.setCharacterEncoding("UTF-8");
    String id = request.getParameter("id"); //Pedimos el id que hemos recibido por GET desde list.jsp

    if (id==null){
        response.sendRedirect("list.jsp");
    }else{
        // recibimos id, puede ser que
        // 1) llegue por GET, desde list.jsp: mostraremos los datos para que puedan ser editados
        // 2) llegue por POST, junto con el resto de datos, para guardar los cambios

        //verificamos si la petición procede de un POST
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            // hemos recibido un POST, reccpilamos el resto de datos...
                
                int id_numerico = Integer.parseInt(id);
                String nombreHospital = request.getParameter("nombre");
                String direccion = request.getParameter("direccion");
                String usuarioHospital = request.getParameter("usuarioHospital");
                String contrasena = request.getParameter("contrasena");

                //creamos nuevo objeto Curso, que reemplazará al actual del mismo id
                hosp = new Hospitales(id_numerico,nombreHospital,usuarioHospital,direccion,contrasena);
                ControllerHospitales.edit(hosp);
                //redirigimos navegador a la página list.jsp
                response.sendRedirect("list.jsp");

        } else {
            // hemos recibido un GET, solo tenemos un id
            // pedimos los datos del Curso para mostrarlos en el formulario de edifión
           hosp = ControllerHospitales.getId(Integer.parseInt(id));
        }
    }
%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Javaterapia</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/academiapp/css/estilos.css">
</head>
<body>
<%-- <%@include file="/partial/menu.html"%> --%>

<div class="container">

<div class="row">
<div class="col">
<h1>Editar Hospital</h1>
</div>
</div>

<div class="row">
<div class="col-md-8">

<form action="#" method="POST">
  <div class="form-group">
    <label for="nombreInput">Nombre del Hospital</label>
    <input  name="nombre"  type="text" class="form-control" id="nombre" value="<%= hosp.getNombreHospital() %>" > 
  </div>
 <div class="form-group">
    <label for="direccionInput">Direccion</label>
    <input  name="direccion"  type="text" class="form-control" id="direccionInput" value="<%= hosp.getDireccion() %>" >
  </div>
  <div class="form-group">
    <label for="usuarioInput">Usuario Hospital</label>
    <input  name="usuarioHospital"  type="text" class="form-control" id="usuarioInput"value="<%= hosp.getUsuarioHospital() %>">
  </div>
    <div class="form-group">
    <label for="contrasena">Contraseña Hospital</label>
    <input  name="contrasena"  type="text" class="form-control" id="contrasena" value="<%= hosp.getContrasena() %> ">
  </div>
  <!-- guardamos id en campo oculto! -->
    <input type="hidden" name="id" value="<%=id %>" >
    <button type="submit" class="btn btn-primary">Guardar</button>
</form>
</div>
</div>
</div>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>
</html>