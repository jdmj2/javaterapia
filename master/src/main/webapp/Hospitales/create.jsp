<%@page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.codesplai.javaterapia.*" %>

<%
    boolean loginRequired=true;
    boolean datosOk;

    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
    // si es un POST...
    if ("POST".equalsIgnoreCase(request.getMethod())) {

        // hemos recibido un POST, deberíamos tener datos del nuevo alumno
       
        request.setCharacterEncoding("UTF-8");

        String direccion = request.getParameter("direccion");
        String nombreHospital = request.getParameter("nombreHospital");
        String usuarioHospital = request.getParameter("usuarioHospital");
        String contrasena = request.getParameter("contrasena");
       // aquí verificaríamos que todo ok, y solo si todo ok hacemos SAVE, 

        datosOk=true;
        if (datosOk){
            Hospitales h = new Hospitales(direccion, nombreHospital, usuarioHospital,  contrasena);
            ControllerHospitales.save(h);
            response.sendRedirect("list.jsp"); 
            return;
        }
    }
%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Javaterapia</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="javaterapia/css/styles.css">
</head>
<body>



<div class="container">
<div class="row">
<div class="col">
<h1>Nuevo Hospital</h1>
</div>
</div>


<div class="row">
<div class="col-md-8">

<form id="formcrea" action="#" method="POST">
  <div class="form-group">
    <label for="nombreInput">Nombre del hospital</label>
    <input  name="nombreHospital"  type="text" class="form-control" id="nombreInput" >
  </div>
    <div class="form-group">
    <label for="claveInput">Contraseña </label>
    <input  name="contrasena"  type="password" class="form-control" id="claveInput" >
  </div>
   <div class="form-group">
    <label for="clave2Input">Contraseña </label>
    <input  name="clave2"  type="password" class="form-control" id="clave2Input" >
  </div> 


 <div class="form-group">
    <label for="direccionInput">Dirección</label>
    <input  name="direccion"  type="text" class="form-control" id="direccionInput">
  </div>
  <div class="form-group">
    <label for="usuarioHospitalInput">Usuario del Hospital</label>
    <input  name="usuarioHospital"  type="text" class="form-control" id="usuarioHospitalInput" >
  </div>
    <button type="submit" class="btn btn-primary">Guardar</button>
</form>


</div>
</div>




    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/javaterapia/js/scripts.js"></script>

<script>

$(document).on("submit", "#formcrea", function(e){

    if ($("#claveInput").val()!=$("#clave2Input").val()) {
        console.log("error claves");
        e.preventDefault();
    }

})

</script>

</body>
</html>