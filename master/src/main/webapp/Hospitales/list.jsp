<%@page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.codesplai.javaterapia.*" %>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Javaterapia</title>
    <!--Favicon-->
    <link rel="icon" href="../favicon.ico">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>
<body >
<%@include file="/partial/menu.jsp"%>
<div class="container">
<div class="row">
<div class="col mt-4">
<h1>Listado</h1>
</div>
</div>
<div class="row">
<div class="col">

<%-- Creamos tabla con Títulos y luego llamamos a nuestra funcion java getAll para mostrar cada objeto con todos sus atributos --%>
<table class="table" id="tabla_listado">
  <thead>
    <tr>
      <th scope="col">#id</th>
      <th scope="col">Nombre Hospital</th>
      <th scope="col">Dirección</th>
      <th scope="col">Usuario Hospital</th>
      <th scope="col">Contraseña</th>
     <th scope="col"></th>
     <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
   <% for (Hospitales hosp : ControllerHospitales.getAll()) { %>
        <tr>
        <th scope="row"><%= hosp.getId() %></th>
        <td><%= hosp.getNombreHospital() %></td>
        <td><%= hosp.getDireccion() %></td>
        <td><%= hosp.getUsuarioHospital() %></td>
        <td><%= hosp.getContrasena() %></td>
        <td><a href="/javaterapia/Hospitales/edit.jsp?id=<%= hosp.getId() %>">Edita</a></td>
        <td><a href="/javaterapia/Hospitales/remove.jsp?id=<%= hosp.getId() %>">Elimina</a></td>
        </tr>
    <% } %>
  </tbody>
</table>

<a href="/javaterapia/Hospitales/create.jsp" class="btn btn-sm btn-danger">Nuevo Hospital</a>
</div>
</div>


</div>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>
</html>
