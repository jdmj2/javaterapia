<%@page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.codesplai.javaterapia.*" %>
<%
    String id = request.getParameter("id");

    if 
        (id==null){
        //no recibimos id, debe ser un error... volvemos a list
        response.sendRedirect("list.jsp");

    }else{
        int id_numerico = Integer.parseInt(id);  //Pasamos el id a Integer
        ControllerHospitales.removeId(id_numerico);  //Llamamos la función removeID con el id pasado a int
        response.sendRedirect("list.jsp");
    }
%>