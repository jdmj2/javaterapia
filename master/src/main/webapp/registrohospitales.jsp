<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.codesplai.javaterapia.*" %>


<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>JavaTerapia | Donamos diversión</title>

    <!--Google Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather|Montserrat|Roboto|Sacramento" rel="stylesheet">
    <!--CSS Stylesheets-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    <!--Favicon-->
    <link rel="icon" href="favicon.ico">
    <!--Font Awesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    
</head>

<body>
<%@include file="/partial/login.jsp"%>
<%@include file="/partial/menu.jsp"%>
<%

        request.setCharacterEncoding("UTF-8");

        if (request.getParameter("idpeticionaeliminar")!= null){
            int idpeticionaeliminar = Integer.parseInt(request.getParameter("idpeticionaeliminar"));
            ControllerPeticiones.updateEstado2(idpeticionaeliminar);
        }
            Hospitales esteHospital = ControllerHospitales.getHospital(loginName, loginPassword);


            if(esteHospital==null)
            {

                response.sendRedirect("/javaterapia/loginhospitales.jsp");
                return;

            }

    %>
    <script>
        var http = new XMLHttpRequest();
        http.onreadystatechange = function () {
            if (http.readyState == 4 && http.status == 200) {
                document.body.innerHTML += http.responseText; // you can change the target. As long as you append http.responseText. It will work fine.
            }
        };
        http.open('GET', 'partial/footer.html', true); // note: link the footer.html
        http.send();
    </script>

    <section id="misPeticiones">
        <div class="container-fluid">
            <h2 class="misPeticiones">Mis peticiones</h2>
            <button type="button" id="displayNuevaPeticion" class="nuevaPeticion btn btn-success" data-toggle="tooltip" data-placement="right"
            title="Nueva Petición">+</button>

            <div class="row">
                <div class="col-12  col-lg-8 ">
    <div class="formPeticiones">
    <form action="registrarpeticion" method="POST" id="formNuevaPeticion" class="hideForm nuevaPeticion">
        <div class="container-fluid">
            <div class="row">
                <div class="form-group col-6">
                    <select id="opcionesCategoria" name="opcionesCategoria" class="custom-select custom-select-lg mb-3">
                        <option selected value="videojuegos">Videojuegos</option>
                        <option value="juegos de mesa">Juegos de Mesa</option>
                        <option value="juegos deportivos">Juegos deportivos</option>
                    </select>
                    <label for="">Descripción de la petición</label>
                    <textarea class="form-control" name="descripcionPeticion" id="descripcionPeticion" rows="3"></textarea>
                    <input type="hidden" name="idHospital" id="idHospital" value="<%= esteHospital.getId()  %>"/>
                    <small>El número máximo de caracteres son de 150.</small>
                    
                    <input type="hidden" name="inputUsuario" id="inputUsuario" value="<%=loginName%>"/>
                    <input type="hidden" name="inputPassword" id="inputPassword" value="<%=loginPassword%>"/>
                </div>
                <div class="form-group col-6">
                    <div class="espacioIconoCategoria"><i id="iconoCategoriaJuego" class="iconoTamañoCompleto"></i>
                    </div>
                </div>


                <div class="form-group col-8">
                    <input class="btn btn-lg btn-info" type="submit" value="Enviar">
                </div>
            </div>
                </form>
        </div>
                       </div>
            </div>
        </div>


        </section>
    <div class="separador"></div>

    <div class="container-fluid">
        <div class="row">

        <% for (Peticiones pet : ControllerPeticiones.getAllPeticiones(esteHospital.getId())) { %>
        <% if  (pet.getEstado()!=2) {   %>
                    <% Hospitales u = ControllerHospitales.getId(pet.getIdHospitales());%>

                    <%  String fichaPendiente = "";
                        String disabled = "";

                        if(pet.getEstado()==1){

                            fichaPendiente = "fichaEnProceso";
                            disabled = "disabled";
                        }


                    %>


                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 mt-3 mb-3">
                        <div class="fichaPeticion <%=fichaPendiente%> card ">
                            <h5 class="card-header"><%= u.getNombreHospital() %></h5>
                            <div class="card-body peticionestarjeta ">
                                <h5 class="card-title"><%= pet.getNombreCategoria()%><i class="fas <%= ControllerPeticiones.getCategoria(pet.getNombreCategoria()) %>"> </i></h5>
                                <p class="card-text "><%= pet.getDescripcion() %></p>
                            </div>
                            <div class="card-body">
                            <a href="#" class="btn btn-lg disabled botonFicha btn-primary">DONAR</a>
                            </div>
                            <form class="" action="" method="POST">
                            <input type="hidden" name="idpeticionaeliminar" value="<%= pet.getId()%>">

                            <input type="submit" class="eliminarPeticion" value="X">
                            </form>
                        </div>
                    </div>
                    <% } } %>
        </div>
    </div>


    <!--Bootstrap Scripts-->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
</body>

</html>
    <%





%>
