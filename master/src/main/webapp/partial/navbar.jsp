<%@page contentType="text/html;charset=UTF-8" %>

<%
  //inicializamos las variables loginId y loginName, por el momento sin valor
  int loginId=0;
  String loginName=null;
  String loginPassword=null;


  if (session.getAttribute("loginId") != null) {

          loginId = (Integer) session.getAttribute("loginId");
          if (loginId>0){
            loginName = (String) session.getAttribute("loginName");
            loginPassword = (String) session.getAttribute("loginPassword");
          }
      }
 

%>

<div style="">
  <nav class="navbar navbar-expand-lg navbar-dark"
    style="background-color: #048998; margin-bottom: 50px; position: fixed; top:0px; width: 100%; z-index: 2;">
    <a class="navbar-brand " style="text-align:center" href="index.html" >Javaterapia</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
      aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">



        <!-- Items de la navbar -->
        
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              Conócenos
            </a>
            <div class="dropdown-menu" aria-labelledby="Inicio">
                <a class="dropdown-item" href="index.html#anchorTop">Nuestro equipo</a>
                <a class="dropdown-item" href="index.html#anchorBottom">Nuestro trabajo</a>
             
            </div>
          </li>
        

          <li class="nav-item"><a href="vistadonante.html" class="nav-link">Realizar Donación</a></li>

        <li class="nav-item"><a href="loginhospitales.html" class="nav-link">Acceso Hospitales</a></li>
        <li class="nav-item">
            <% if (loginId<1) { %>
                <a class="nav-link" href="#" data-toggle="modal" data-target="#loginModal">
                Login
                </a>
            <% } else { %>
            <form action="#" method="POST" id="logoutForm">
                <input type="hidden" name="logoutpost" value="modal">
                <a class="nav-link" id="logoutFormLaunch" href="#">Logout <%= loginName %></a>
              </form>
            <% } %>
        </li>
      </ul>
    </div>
  </nav>
</div>

<!-- Copiar esto donde se desee poner el navbar
<div style="margin-bottom:50px;">
        <script>
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function(){
                    if (xhttp.readyState == 4 && xhttp.status == 200){
                        document.body.innerHTML += xhttp.responseText; // you can change the target. As long as you append xhttp.responseText. It will work fine.
                    }
                };
                xhttp.open('GET', 'partial/navbar.html', true); // note: link the footer.html
                xhttp.send();
            </script>
            </div>

     -->