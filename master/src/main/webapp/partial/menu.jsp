<%@page contentType="text/html;charset=UTF-8" %>

<%
  //inicializamos las variables loginId y loginName, por el momento sin valor
  int loginId=0;
  String loginName=null;
  String loginPassword=null;


  if (session.getAttribute("loginId") != null) {

          loginId = (Integer) session.getAttribute("loginId");
          if (loginId>0){
            loginName = (String) session.getAttribute("loginName");
            loginPassword = (String) session.getAttribute("loginPassword");
          }
      }


%>

<nav class="navbar menuNavBar navbar-expand-lg  navbar-dark bg-primary" style="background-color: #048998 !important;
  margin-bottom: 50px; position: fixed; top:0px; width: 100%; z-index: 2;">
    <a class="navbar-brand" href="index.jsp"><img src="img/index.png" width="30px" height="30px"/></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              Conócenos
            </a>
            <div class="dropdown-menu" aria-labelledby="Inicio">
              <a class="dropdown-item" href="index.jsp#nEquipo">Nuestro equipo</a>
              <a class="dropdown-item" href="index.jsp#nTrabajo">Nustro trabajo</a>

            </div>
          </li>

            
        <li class="nav-item"><a href="vistadonante.jsp?hospital=0&categoria_id=nada" class="nav-link">Realizar Donación</a></li>

        <li class="nav-item">
        <% if (loginId<1) { %>
            <a href="loginhospitales.jsp" class="nav-link">Acceso Hospitales</a>
        <% }else{ %>
            <a href="registrohospitales.jsp" class="nav-link">Mis Peticiones</a>
        <% } %>
        </li>



         <li style="position: absolute; right: 20px !important;" class="nav-item">
            <% if (loginId<1) { %>

            <% } else { %>
            <form action="#" method="POST" id="logoutForm">
                <input type="hidden" name="logoutpost" value="modal">
                <a style="cursor: pointer;" onClick='$("#logoutForm").submit()' class="nav-link" id="logoutFormLaunch" >Logout <%= loginName %></a>
              </form>
            <% } %>
        </li>



      </ul>




    </div>
  </nav>
