<%@page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.codesplai.javaterapia.*" %>


<%
    request.setCharacterEncoding("UTF-8");
    int idPeticiones = Integer.parseInt(request.getParameter("idpeticion"));
    String nombreDonante = request.getParameter("nombreDonante");
    String emailDonante = request.getParameter("emailDonante");

    Donaciones estaDonacion = new Donaciones(nombreDonante, emailDonante, idPeticiones);
    
    ControllerDonaciones.saveDonaciones(estaDonacion);
    ControllerPeticiones.updateEstado1(idPeticiones);

%>

<!DOCTYPE html>
<html>

        <head>
                <meta charset="utf-8">
                <title>JavaTerapia | Donamos diversión</title>
            
                <!--Google Fonts-->
                <link href="https://fonts.googleapis.com/css?family=Merriweather|Montserrat|Roboto|Sacramento" rel="stylesheet">
                <!--CSS Stylesheets-->
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
                    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
                <link rel="stylesheet" href="css/styles.css">
                <!--Favicon-->
                <link rel="icon" href="favicon.ico">
                <!--Font Awesome-->
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
                    integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
            </head>
<body>
<%@include file="/partial/login.jsp"%>
<%@include file="/partial/menu.jsp"%>
    <div id="graciasArriba" class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-sm-10 d-none d-sm-block">

                <!-- <i class="fas fa-thumbs-up"></i> -->
                <img  src="img/GraiasJavaTerapia.png" alt="..." style="margin-top:280px;width:300%;height:60%;">

            </div>
        </div>
    </div>

    <div id="graciasAbajo" class="container-fluid mt-5">

        <div class="row">
            <div class="col mt-5">
                <h2 class="graciasCol">¡Gracias por su colaboración!</h2> 
         <br>
                <button style="border-radius: 20px; width: 200px; box-shadow: 3px 3px 4px grey;" type="button" class="btn btn-info"><a href="index.jsp"  style="color:white; text-decoration:none">Inicio</a></button>
            </div>
        </div>
    </div>
    <i style="font-size: 2000%; color: green; position: relative; bottom: 300px; left: 100px;" class="fas d-block d-sm-none fa-thumbs-up"></i>
    



        <script>
                var http = new XMLHttpRequest();
                http.onreadystatechange = function(){
                    if (http.readyState == 4 && http.status == 200){
                        document.body.innerHTML += http.responseText; // you can change the target. As long as you append http.responseText. It will work fine.
                    }
                };
                http.open('GET', 'partial/footer.html', true); // note: link the footer.html
                http.send();
            </script>
         

    <!--Bootstrap Scripts-->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
</body>



</html>