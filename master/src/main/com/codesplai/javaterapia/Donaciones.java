package com.codesplai.javaterapia;

public class Donaciones {

    private int idDonaciones;
    private String nombreDonante;
    private String correoDonante;

    public Donancion (String nombreDonante, String correoDonante){
        this.nombreDonante = nombreDonante;
        this.correoDonante = correoDonante;
    }

    public Donacion (int idDonaciones, String nombreDonante, String correoDonante){
        this.idDonaciones = idDonaciones;
        this.nombreDonante = nombreDonante;
        this.correoDonante = correoDonante;
    }

    public String getNombreDonante() {
        return this.nombreDonante;
    }

    protected void setNombreDonante(String nombreDonante) {
        this.nombreDonante = nombreDonante;
    }

    public String getCorreoDonante() {
        return this.correoDonante;
    }

    protected void setCorreoDonante(String correoDonante) {
        this.correoDonante = correoDonante;
    }

    public int getidDonaciones() {
        return this.idDonaciones;
    }

    protected void setidDonaciones(int idDonaciones) {
        this.idDonaciones = idDonaciones;
    }
}
