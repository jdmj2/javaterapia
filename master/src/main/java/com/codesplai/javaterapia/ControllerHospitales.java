package com.codesplai.javaterapia;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class ControllerHospitales {

    // constantes utilizadas en las ordenes sql
    private static final String TABLE = "hospitales";
    private static final String KEY = "idHospitales";

    // Array para poder llamarla despues en funciones como getAll
    private static List<Hospitales> listaHospitales = new ArrayList<Hospitales>();
    private static int contador = 0;

    // miramos si el usuario y la password coinciden con esta función
    public static int checkLogin(String userHosp, String passHosp) {
        int resId = 0;

        String sql = String.format("select %s from %s where usuarioHospital=? and contrasena=?", KEY, TABLE);
        try (Connection conn = DBConn.getConn(); PreparedStatement stmt = conn.prepareStatement(sql)) {

            stmt.setString(1, userHosp);
            stmt.setString(2, passHosp);
            System.out.println(stmt.toString());

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                resId = 1;
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return resId;
    }

    /* devuelve un registro a partir de las variables de usuario */
    public static Hospitales getHospital(String userHosp, String passHosp) {
        Hospitales u = null;

        String sql = String.format(
                "select idHospitales, direccion,nombreHospital,usuarioHospital, contrasena from %s where usuarioHospital='%s' and contrasena=\"%s\"",
                TABLE, userHosp, passHosp);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Hospitales(rs.getInt("idHospitales"), rs.getString("direccion"), rs.getString("nombreHospital"),
                        rs.getString("usuarioHospital"), rs.getString("contrasena"));
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }

    // Recibimos un ID y devolvemos todos los datos relaciones con el hospital con
    // ese ID
    public static Hospitales getId(int id) {
        String sql = "SELECT idHospitales, direccion, nombreHospital, usuarioHospital, contrasena FROM hospitales WHERE idHospitales="
                + id;
        Hospitales hospital_sel = null;

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement();) {

            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                hospital_sel = new Hospitales(rs.getInt("idHospitales"), rs.getString("direccion"),
                        rs.getString("nombreHospital"), rs.getString("usuarioHospital"), rs.getString("contrasena"));

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return hospital_sel;
    }

    // Creamos una lista con todos los hospitales y sus características
    public static List<Hospitales> getAll() {

        List<Hospitales> listaHospitales = new ArrayList<Hospitales>();

        {

            String sql = String.format("SELECT * FROM hospitales;", KEY, TABLE);

            try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) { // (3)

                ResultSet rs = stmt.executeQuery(sql); // (4)
                while (rs.next()) {
                    Hospitales u = new Hospitales((Integer) rs.getObject(1), (String) rs.getObject(2),
                            (String) rs.getObject(3), (String) rs.getObject(4), (String) rs.getObject(5)); // (5)
                    listaHospitales.add(u);
                }

            } catch (Exception e) { // (6)
                String s = e.getMessage();
                System.out.println(s);
            }
            return listaHospitales;
        }
    }

    // Funcion para añadir nuevos hospitales. Recibe las 4 keys en inputs y le
    // asigna un nuevo ID
    public static void save(Hospitales hospital) {
        String sql = "INSERT INTO hospitales (direccion, nombreHospital, usuarioHospital, contrasena) VALUE (?,?,?,?)";

        try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql);) {

            pstmt.setString(1, hospital.getDireccion());
            pstmt.setString(2, hospital.getNombreHospital());
            pstmt.setString(3, hospital.getUsuarioHospital());
            pstmt.setString(4, hospital.getContrasena());
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // Elimina un hospital y sus características con el id que le proporcionemos
    // mediante un GET
    public static void removeId(int id) {
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void edit(Hospitales hospital) {
        String sql = "UPDATE hospitales SET direccion=?, nombreHospital=?, usuarioHospital=?, contrasena=? WHERE idHospitales=?";

        try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql);) {

            pstmt.setString(2, hospital.getDireccion());
            pstmt.setString(3, hospital.getNombreHospital());
            pstmt.setString(1, hospital.getUsuarioHospital());
            pstmt.setString(4, hospital.getContrasena());
            pstmt.setInt(5, hospital.getId());
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
