package com.codesplai.javaterapia;

import java.text.SimpleDateFormat;
import java.util.*;

public class Peticiones {

    private int idPeticiones;
    private String nombreCategoria;
    private String fechaAlta;
    private int estado;
    private String descripcion;
    private int idHospitales;

    // CONSTRUCTOR SIN ID

    public Peticiones(String nombre, String fechaAlta, int estado, String descripcion) {

        this.nombreCategoria = nombre;
        this.fechaAlta = fechaAlta;
        this.estado = estado;
        this.descripcion = descripcion;
    }

    // CONSTRUCTOR CON ID

    public Peticiones(String nombreCategoria, String descrpcion, int idHospitales) {

        Date fechaActual = new Date();

        this.estado = 0;
        this.fechaAlta = fechaActual.toString();
        this.nombreCategoria = nombreCategoria;
        this.descripcion = descrpcion;
        this.idHospitales = idHospitales;

    }

    public Peticiones(int id, String nombre, String fechaAlta, int estado, String descripcion) {

        this.idPeticiones = id;
        this.nombreCategoria = nombre;
        this.fechaAlta = fechaAlta;
        this.estado = estado;
        this.descripcion = descripcion;
    }

    public Peticiones(int id, String nombre, String fechaAlta, int estado, String descripcion, int idHospitales) {

        this.idPeticiones = id;
        this.nombreCategoria = nombre;
        this.fechaAlta = fechaAlta;
        this.estado = estado;
        this.descripcion = descripcion;
        this.idHospitales = idHospitales;
    }

    // GETTERS

    public int getId() {
        return this.idPeticiones;
    }

    public String getNombreCategoria() {
        return this.nombreCategoria;
    }

    public String getFechaAlta() {
        return this.fechaAlta;
    }

    public int getIdHospitales() {
        return this.idHospitales;
    }

    public int getEstado() {
        return this.estado;
    }

    // SETTERS

    protected void setId(int id) {
        this.idPeticiones = id;
    }

    protected void setNombreCategoria(String nombre) {
        this.nombreCategoria = nombre;
    }

    protected void setFechaAlta(String fecha) {
        this.fechaAlta = fecha;
    }

    protected void setEstado(int estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    protected void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    protected void setIdHospitales(int idHospital) {
        this.idHospitales = idHospital;
    }
}