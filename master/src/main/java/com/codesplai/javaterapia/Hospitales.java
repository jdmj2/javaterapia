package com.codesplai.javaterapia;

import java.util.*;

public class Hospitales {

    private int idHospitales;
    private String direccion;
    private String nombreHospital;
    private String usuarioHospital;
    private String contrasena;

    /* Constructor por defecto */

    public Hospitales() {
    }

    /* Constructor sin id */
    public Hospitales(String direccion, String nombreHospital, String usuarioHospital, String contrasena) {
        this.direccion = direccion;
        this.nombreHospital = nombreHospital;
        this.usuarioHospital = usuarioHospital;
        this.contrasena = contrasena;
    }

    /* Constructor con id */
    public Hospitales(int idHospitales, String direccion, String nombreHospital, String usuarioHospital,
            String contrasena) {
        this.idHospitales = idHospitales;
        this.direccion = direccion;
        this.nombreHospital = nombreHospital;
        this.usuarioHospital = usuarioHospital;
        this.contrasena = contrasena;
    }

    /* Getters */
    public int getId() {
        return this.idHospitales;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public String getNombreHospital() {
        return this.nombreHospital;
    }

    public String getUsuarioHospital() {
        return this.usuarioHospital;
    }

    public String getContrasena() {
        return this.contrasena;
    }

    /* Setters */
    protected void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    protected void setUsuarioHospital(String usuarioHospital) {
        this.usuarioHospital = usuarioHospital;
    }

    protected void setNombreHospital(String nombreHospital) {
        this.nombreHospital = nombreHospital;
    }

    protected void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    protected void setIdHospitales(int idHospitales) {
        this.idHospitales = idHospitales;
    }
}
