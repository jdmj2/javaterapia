package com.codesplai.javaterapia;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class ControllerPeticiones {

    public static final String KEY = "idPeticiones";
    public static final String TABLE = "peticiones";

    public static List<Peticiones> getAll() {

        List<Peticiones> listaPeticiones = new ArrayList<Peticiones>();

        String sql = String.format(
                "select %s, nombreCategoria, fechaAlta, estado, Descripcion, Hospitales_idHospitales  from %s", KEY,
                TABLE);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Peticiones u = new Peticiones(

                        rs.getInt(KEY), rs.getString("nombreCategoria"), rs.getString("fechaAlta"), rs.getInt("estado"),
                        rs.getString("Descripcion"), rs.getInt("Hospitales_idHospitales")

                );
                listaPeticiones.add(u);
            }
        } catch (Exception e) {
            String s = e.getMessage();
        }

        return listaPeticiones;

    }

    public static List<Peticiones> getAllPeticiones(int idHospital) {

        List<Peticiones> listaPeticiones = new ArrayList<Peticiones>();
        
            String sql = String.format(
                    "select %s, nombreCategoria, fechaAlta, estado, Descripcion, Hospitales_idHospitales  from %s where Hospitales_idHospitales ='%s'",
                    KEY, TABLE, idHospital);

            try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    Peticiones u = new Peticiones(

                            rs.getInt(KEY), rs.getString("nombreCategoria"), rs.getString("fechaAlta"),
                            rs.getInt("estado"), rs.getString("Descripcion"), rs.getInt("Hospitales_idHospitales")

                    );
                    listaPeticiones.add(u);
                }
            } catch (Exception e) {
                String s = e.getMessage();
                System.out.println(s);
            }

            return listaPeticiones;
        
    }


    public static List<Peticiones> getAllPeticiones(int idHospital, String categoria) {

        List<Peticiones> listaPeticiones = new ArrayList<Peticiones>();
        
         if (idHospital == 0 && categoria.equals("nada")) {
            String sql = String.format(
                    "select %s, nombreCategoria, fechaAlta, estado, Descripcion, Hospitales_idHospitales  from %s", KEY,
                    TABLE);

            try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    Peticiones u = new Peticiones(

                            rs.getInt(KEY), rs.getString("nombreCategoria"), rs.getString("fechaAlta"),
                            rs.getInt("estado"), rs.getString("Descripcion"), rs.getInt("Hospitales_idHospitales"));
                    listaPeticiones.add(u);
                }
            } catch (Exception e) {
                String s = e.getMessage();
            }

            return listaPeticiones;

        }
        else if (categoria.equals("nada")) {
            String sql = String.format(
                    "select %s, nombreCategoria, fechaAlta, estado, Descripcion, Hospitales_idHospitales  from %s where Hospitales_idHospitales ='%s'",
                    KEY, TABLE, idHospital);

            try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    Peticiones u = new Peticiones(

                            rs.getInt(KEY), rs.getString("nombreCategoria"), rs.getString("fechaAlta"),
                            rs.getInt("estado"), rs.getString("Descripcion"), rs.getInt("Hospitales_idHospitales")

                    );
                    listaPeticiones.add(u);
                }
            } catch (Exception e) {
                String s = e.getMessage();
            }

            return listaPeticiones;

        } else if (idHospital == 0) {

            String sql = String.format(
                    "select %s, nombreCategoria, fechaAlta, estado, Descripcion, Hospitales_idHospitales  from %s where nombreCategoria ='%s'",
                    KEY, TABLE, categoria);

            try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    Peticiones u = new Peticiones(

                            rs.getInt(KEY), rs.getString("nombreCategoria"), rs.getString("fechaAlta"),
                            rs.getInt("estado"), rs.getString("Descripcion"), rs.getInt("Hospitales_idHospitales")

                    );
                    listaPeticiones.add(u);
                }
            } catch (Exception e) {
                String s = e.getMessage();
            }

            return listaPeticiones;

        }

        

        else

        {
            String sql = String.format(
                    "select %s, nombreCategoria, fechaAlta, estado, Descripcion, Hospitales_idHospitales  from %s where Hospitales_idHospitales ='%s' and nombreCategoria='%s'",
                    KEY, TABLE, idHospital, categoria);

            try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    Peticiones u = new Peticiones(

                            rs.getInt(KEY), rs.getString("nombreCategoria"), rs.getString("fechaAlta"),
                            rs.getInt("estado"), rs.getString("Descripcion"), rs.getInt("Hospitales_idHospitales")

                    );
                    listaPeticiones.add(u);
                }
            } catch (Exception e) {
                String s = e.getMessage();
            }

            return listaPeticiones;

        }
    }

    public static void savePeticion(Peticiones peticion) {
        String sql = String.format(
                "INSERT INTO %s (nombreCategoria, fechaAlta, estado, Descripcion, Hospitales_idHospitales) VALUES (?,?,?,?,?)",
                TABLE);
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {

            pstmt.setString(1, peticion.getNombreCategoria());
            pstmt.setString(2, peticion.getFechaAlta().toString());
            pstmt.setInt(3, peticion.getEstado());
            pstmt.setString(4, peticion.getDescripcion());
            pstmt.setInt(5, peticion.getIdHospitales());
            pstmt.executeUpdate();

        } catch (Exception e) {
            String s = e.getMessage();
        }

    }

    public static void updateEstado1(int idPeticion) {

        String sql = "UPDATE peticiones SET estado=1 WHERE idPeticiones=" + idPeticion;

        try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql);) {

            pstmt.executeUpdate();

        } catch (Exception e) {
        }
    }

    public static void updateEstado2(int idPeticion) {

        String sql = "UPDATE peticiones SET estado=2 WHERE idPeticiones=" + idPeticion;

        try (Connection conn = DBConn.getConn(); PreparedStatement pstmt = conn.prepareStatement(sql);) {

            pstmt.executeUpdate();

        } catch (Exception e) {
        }
    }

    public static String getCategoria(String categoria) {
        String claseIcono = "";

        if (categoria.equals("videojuegos")) {
            claseIcono = "fa-gamepad";
        } else if (categoria.equals("juegos de mesa")) {
            claseIcono = "fa-dice";
        } else if (categoria.equals("juegos deportivos")) {
            claseIcono = "fa-futbol";
        }

        return claseIcono;
    }

    public static Hospitales getId(int id) {
        String sql = "SELECT idPeticiones, nombreCategoria, fechaAlta, estado, Descripcion FROM peticiones WHERE idPeticiones="
                + id;
        Hospitales peticiones_Sel = null;

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement();) {

            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                peticiones_Sel = new Hospitales(rs.getInt("idHospitales"), rs.getString("direccion"),
                        rs.getString("nombreHospital"), rs.getString("usuarioHospital"), rs.getString("contrasena"));

            }
        } catch (Exception e) {
        }
        return peticiones_Sel;
    }

    public static List<Peticiones> getPeticionById(int idPeticion) {

        List<Peticiones> listaPeticiones = new ArrayList<Peticiones>();
        String sql = String.format(
                "select %s, nombreCategoria, fechaAlta, estado, Descripcion, Hospitales_idHospitales  from %s where idPeticiones ='%s'",
                KEY, TABLE, idPeticion);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Peticiones u = new Peticiones(

                        rs.getInt(KEY), rs.getString("nombreCategoria"), rs.getString("fechaAlta"), rs.getInt("estado"),
                        rs.getString("Descripcion"), rs.getInt("Hospitales_idHospitales")

                );
                listaPeticiones.add(u);
            }
        } catch (Exception e) {
            String s = e.getMessage();
        }

        return listaPeticiones;
    }
}