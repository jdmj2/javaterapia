package com.codesplai.javaterapia;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class ServletRegistrarPeticion extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        HttpSession session = request.getSession();

        session.getAttribute("usuario");
        session.getAttribute("contrasena");

        String usuarioLogin = request.getParameter("inputUsuario");
        String contrasenaLogin = request.getParameter("inputPassword");

        String categoriaJuego = request.getParameter("opcionesCategoria");
        String descripcionPeticion = request.getParameter("descripcionPeticion");
        int idHospitalPeticion = Integer.parseInt(request.getParameter("idHospital"));

        Peticiones fichaPeticion = new Peticiones(categoriaJuego, descripcionPeticion, idHospitalPeticion);

        // ESTO REGISTRA UNA NUEVA PETICION EN LA BBDD - EL OBJETIVO DE ESTE SERVLET
        ControllerPeticiones.savePeticion(fichaPeticion);

        // VUELVE A LA PÁGINA DE REGISTRO/VISTA DE PETICIONES
        response.sendRedirect("registrohospitales.jsp");

    }
}