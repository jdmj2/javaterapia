package com.codesplai.javaterapia;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class ControllerDonaciones {

    private static final String TABLE = "donaciones";
    private static final String KEY = "idDonaciones";

    public static void saveDonaciones(Donaciones donacion) {
        String sql = String
                .format("INSERT INTO %s (nombreDonante, correoDonante, Peticiones_idPeticiones) VALUES (?,?,?)", TABLE);
        System.out.println(sql);
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {

            pstmt.setString(1, donacion.getNombreDonante());
            pstmt.setString(2, donacion.getCorreoDonante().toString());
            pstmt.setInt(3, donacion.getidPeticiones());
            pstmt.executeUpdate();

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }

    }

}