package com.codesplai.javaterapia;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class ServletLogoutHospital extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        
        //RECUPERA EL OBJETO DE LA SESION Y LE ASIGNA LAS VARIABLES DE SESION EN UN OBJETO DE SESION
        HttpSession session = request.getSession();
        
        session.invalidate();

        //VUELVE A LA PÁGINA DE REGISTRO/VISTA DE PETICIONES
        response.sendRedirect(request.getContextPath() + "/index.html");

    }
}