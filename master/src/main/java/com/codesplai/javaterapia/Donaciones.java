package com.codesplai.javaterapia;

public class Donaciones {

    private int idDonaciones;
    private String nombreDonante;
    private String correoDonante;
    private int idPeticiones;

    public Donaciones(String nombreDonante, String correoDonante, int idPeticiones) {
        this.nombreDonante = nombreDonante;
        this.correoDonante = correoDonante;
        this.idPeticiones = idPeticiones;
    }

    public Donaciones(int idDonaciones, String nombreDonante, String correoDonante, int idPeticiones) {
        this.idDonaciones = idDonaciones;
        this.nombreDonante = nombreDonante;
        this.correoDonante = correoDonante;
        this.idPeticiones = idPeticiones;
    }

    // GETTERS
    public String getNombreDonante() {
        return this.nombreDonante;
    }

    public String getCorreoDonante() {
        return this.correoDonante;
    }

    public int getidDonaciones() {
        return this.idDonaciones;
    }

    public int getidPeticiones() {
        return this.idPeticiones;
    }

    // SETTERS
    protected void setNombreDonante(String nombreDonante) {
        this.nombreDonante = nombreDonante;
    }

    protected void setCorreoDonante(String correoDonante) {
        this.correoDonante = correoDonante;
    }

    protected void setidDonaciones(int idDonaciones) {
        this.idDonaciones = idDonaciones;
    }

    protected void setidPeticiones(int idPeticiones) {
        this.idPeticiones = idPeticiones;
    }
}